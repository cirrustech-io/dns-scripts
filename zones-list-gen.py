#!/usr/bin/python
import re
import sys
import os
from variables import *
import sys
import logging
from logging.handlers import SysLogHandler

PID = str(sys.argv[1])

#Defining Logger
logger = logging.getLogger()
logger.addHandler(SysLogHandler('/dev/log'))

logger.warn("ZONE-LIST-GEN[%s]: Sub-script started" % (PID))

if os.path.exists(temp_src_rev4_zones_list):
  os.rename(temp_src_rev4_zones_list, temp_src_rev4_zones_list_old)
  open(temp_src_rev4_zones_list, "w").write(open(src_rev4_zones_list).read())
else:
  open(temp_src_rev4_zones_list, "w").write(open(src_rev4_zones_list).read())

#Generate zone list file for NSD (nsd.zone.list)
#forward zones
with open(nsd_zones_list_file, "w") as currentFile:
  for line in open(src_fwd_zones_list):
    zoneName=line.strip()
    currentFile.write('''zone:\n''')
    currentFile.write(''' name: "%s"\n''' % (zoneName))
    currentFile.write(''' zonefile: "%s%s"\n''' % (path_to_fwd_zones, zoneName)) 
    currentFile.write('''\n''')

#reverse ipv4 zones
with open(nsd_zones_list_file, "a") as currentFile:
  for line in open(src_rev4_zones_list):
    fileName=line.strip()
    splitName=fileName.split(".")
    zoneName="%s.%s.%s" % (splitName[2], splitName[1], splitName[0])
    currentFile.write('''zone:\n''')
    currentFile.write(''' name: "%s.in-addr.arpa"\n''' % (zoneName))
    currentFile.write(''' zonefile: "%s%s"\n''' % (path_to_rev_ipv4_zones, fileName)) 
    currentFile.write('''\n''')

#reverse ipv6 zones
with open(nsd_zones_list_file, "a") as currentFile:
  for line in open(src_rev6_zones_list):
    fileName=line.strip()
    zoneName='.'.join(map(str,list(re.sub(':','',fileName)[::-1])))
    currentFile.write('''zone:\n''')
    currentFile.write(''' name: "%s.ip6.arpa"\n''' % (zoneName))
    currentFile.write(''' zonefile: "%s%s"\n''' % (path_to_rev_ipv6_zones, fileName)) 
    currentFile.write('''\n''')

logger.warn("ZONE-LIST-GEN[%s]: Created %s" % (PID, nsd_zones_list_file))

#Generate zone list file for BIND (named.zone.list)
#forward zones
with open(bind_zones_list_file, "w") as currentFile:
  for line in open(src_fwd_zones_list):
    zoneName=line.strip()
    currentFile.write('''zone "%s" {\n''' % (zoneName))
    currentFile.write(''' type master;\n''')
    currentFile.write(''' file "%s%s";\n''' % (path_to_fwd_zones, zoneName)) 
    currentFile.write('''};\n''')
    currentFile.write('''\n''')

#reverse ipv4 zones
with open(bind_zones_list_file, "a") as currentFile:
  for line in open(src_rev4_zones_list):
    fileName=line.strip()
    splitName=fileName.split(".")
    zoneName="%s.%s.%s" % (splitName[2], splitName[1], splitName[0])
    currentFile.write('''zone "%s.in-addr.arpa" {\n''' % (zoneName))
    currentFile.write(''' type master;\n''')
    currentFile.write(''' file "%s%s";\n''' % (path_to_rev_ipv4_zones, fileName)) 
    currentFile.write('''};\n''')
    currentFile.write('''\n''')

#reverse ipv6 zones
with open(bind_zones_list_file, "a") as currentFile:
  for line in open(src_rev6_zones_list):
    fileName=line.strip()
    zoneName='.'.join(map(str,list(re.sub(':','',fileName)[::-1])))
    currentFile.write('''zone "%s.ip6.arpa" {\n''' % (zoneName))
    currentFile.write(''' type master;\n''')
    currentFile.write(''' file "%s%s";\n''' % (path_to_rev_ipv6_zones, fileName)) 
    currentFile.write('''};\n''')
    currentFile.write('''\n''')

logger.warn("ZONE-LIST-GEN[%s]: Created %s" % (PID, bind_zones_list_file))
logger.warn("ZONE-LIST-GEN[%s]: Sub-script exiting normally" % (PID))
sys.exit(0)
