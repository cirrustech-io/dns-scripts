#!/usr/bin/python
# -*- coding: utf-8 -*-

#Author: Mohamed Waqqas, mohamed.waqqas@infonas.com
#Date: 2016-07-15

import re
import os
import time
import datetime
from logging.handlers import SysLogHandler
import logging
import subprocess
from subprocess import call
import sys
import hashlib
from variables import *

#currentTime=str(sys.argv[1])
#print currentTime

#Setting up logging to syslog
#PID = os.getpid()
unixTime = str(sys.argv[1])
humanTimeStamp = str(datetime.datetime.now())
PID = unixTime
logger = logging.getLogger()
logger.addHandler(SysLogHandler('/dev/log'))

#if required uncomment to log to separate file
#logger.addHandler(logging.FileHandler("/home/waqqas/python/syslog"))
logger.warn("DNS-CRON-REV4[%s]: Starting script to generate PTR records" % (PID))


countPTR = 0
countIP = 0
countError = 0
countFile = 0   #Used to keep track of number of config files in git repo

### Find lines like: "interface GigabitEthernet0/0/1"
def findIntName():
    intLine = re.search(r'(^interface )(\D+)(\d.*$)', line)
    if intLine is not None:
        intName = intLine.group(2)[0:2].lower()
        intNum = intLine.group(3).replace('/','-').replace('.','-')
        intComp = intName+intNum
        return intComp

#### Find lines like:
#ip address 192.168.8.0 255.255.255.0
#ip address 80.88.240.1 255.255.255.0
#ip address 46.29.56.1 255.255.255.0
#ip address 188.137.0.0 255.255.255.0
def findIpAddr():
    ipLine = \
        re.search(r'(^ ip address )((80.88.\d+.\d+)|(188.137.\d+.\d+)|(46.29.\d+.\d+))( \d+.\d+.\d+.\d+$)', line)
    if ipLine is not None:
        ipAddr = ipLine.group(2)
        return ipAddr

### Find lines like: " ip address 192.168.1.2 255.255.255.0 secondary"
def findSecIpAddr():
    secIpLine = re.search(r'(^ ip address )((80.88.\d+.\d+)|(188.137.\d+.\d+)|(46.29.\d+.\d+))( \d+.\d+.\d+.\d+)( secondary$)', line)
    if secIpLine is not None:
        secIpAddr = secIpLine.group(2)
        return secIpAddr

#### Find: "!"
def findBang():
    p = re.search(r'(^!$)', line)
    if p is not None:
        return 1

#### Find hostname
def findHostname():
    hostnameLine = re.search(r'(^hostname )(.*$)', line)
    if hostnameLine is not None:
        hostname = hostnameLine.group(2)
        return hostname

### Making sure the tempzones dir exists
if not os.path.exists(tempZoneDir):
  os.makedirs(tempZoneDir)

### If list of all ipv4 ptr records exists from previous run
### Then, rename it with '.old' suffix and create a new one
if os.path.exists(all_ptr_ref_list_path):
  os.rename(all_ptr_ref_list_path, all_ptr_ref_list_path_old)
  open(all_ptr_ref_list_path, "w")
else:
  open(all_ptr_ref_list_path, "w")

#Parse each filename in working dir to 
#find only files that end in .config-backup 
#and assign ASN based upon their management IP address
for (root, dirs, files) in os.walk(run_config_dir):
    asn = ''
    for eachFile in files:
        countFile = countFile+1
        find_1921683 = re.search(r'(^.*192.168.3.*.config-backup$)', eachFile)
        if find_1921683 is not None:
            asn = infonas_asn

        find_1921688 = re.search(r'(^.*192.168.8.*.config-backup$)', eachFile)
        if find_1921688 is not None:
            asn=infonas_uk_asn

        find_8088 = re.search(r'(^.*80.88.*.config-backup$)', eachFile)
        if find_8088 is not None:
            asn = infonas_asn

        find_4629 = re.search(r'(^.*46.29.*.config-backup$)', eachFile)
        if find_4629 is not None:
            asn = infonas_uk_asn

#find hostname from each eligibile file
#then, in each file find interface
#if interface found then look for 
#"!" then "Secondary IP" then "Primary IP"

        with open(root + "/" +eachFile) as myFile:
            for (i, line) in enumerate(myFile, 1):
                if findHostname() is not None:
                    host = findHostname().replace("_","-")
                    j = i + 1

                    for (j, line) in enumerate(myFile, j):
                        if findIntName() is not None:
                            interface = findIntName()
                            k = j + 1

#After an interface has been found,
#look for "!", if found BREAK
#Otherwise, look for Seconday IP Address

                            for (k, line) in enumerate(myFile, k):
                              if findBang() is not None:
                                break

#Find secondary ip address, if it exists
#Don't break if secondary IP address found record it in Dict,
#And start looking for Primary IP Address
#as it always comes after a secondary IP address

                              if findSecIpAddr() is not None:
                                secIp = findSecIpAddr()
                                secIpSplit = secIp.split(".")
                                sec_ip_dashed = secIp.replace(".", '-')+"-sec"
                                secZoneName = "%s.%s.%s" % (secIpSplit[0], secIpSplit[1], secIpSplit[2])
                                dict[secIp] = (secZoneName, interface, host, asn, sec_ip_dashed, secIpSplit[0], secIpSplit[1], secIpSplit[2], secIpSplit[3])

#When Primary IP address is found
#record it in Dict and BREAK
#restart loop on next lines to look for interface again

                              if findIpAddr() is not None:
                                ip = findIpAddr()
                                ipSplit = ip.rsplit('.')
                                ip_dashed = ip.replace('.','-')
                                zoneName = "%s.%s.%s" % (ipSplit[0], ipSplit[1], ipSplit[2])
                                dict[ip] = {}
                                dict[ip] = (zoneName, interface, host, asn, ip_dashed, ipSplit[0], ipSplit[1], ipSplit[2], ipSplit[3])
                                #dict[ip]= (---0----, ----1----, -2--, -3-, ----4----, ---5------, ----6-----, ----7-----, ----8-----)
                                break

#Create all zones from src_rev4_zones_list again and
#Add basic SOA and NS info in all reverse delegation zones
for zoneNameLine in open(src_rev4_zones_list):
  with open(tempZoneDir+zoneNameLine.strip(), "w") as currentFile:
    nameSplit = zoneNameLine.strip().split(".")
    currentFile.write("$ORIGIN %s.%s.%s.in-addr.arpa.\n" % (nameSplit[2], nameSplit[1], nameSplit[0]))
    currentFile.write("$TTL 3600\n")
    currentFile.write("\n")
    currentFile.write("@ \t IN SOA \t ns1.infonas.com. hostmaster.infonas.com. ( %s 21600 3600 1209600 10800 )\n" % (unixTime))
    currentFile.write("\n")
    currentFile.write(" \t IN NS \t\t ns1.infonas.com.\n")
    currentFile.write(" \t IN NS \t\t ns2.infonas.com.\n")
    currentFile.write(" \t IN NS \t\t ns3.infonas.com.\n")
    currentFile.write("\n")
#If the dictionary contains an IP address belonging to the zone
#Then create a PTR record
    new_dict={}
    for ip in dict:
      if dict[ip][0] == zoneNameLine.strip():
        new_dict[dict[ip][8]]=dict[ip]
    new_list = sorted(new_dict)
    new_list.sort(key=int)

    for host in new_list:
      currentFile.write("%s \t IN PTR \t %s.%s.%s.%s.net.\n" % (new_dict[host][8], new_dict[host][1], new_dict[host][4], new_dict[host][2], new_dict[host][3]))
      f = open(all_ptr_ref_list_path, "a")
      f.write("%s \t IN PTR \t %s.%s.%s.%s.net.\n" % (new_dict[host][8], new_dict[host][1], new_dict[host][4],new_dict[host][2],new_dict[host][3]))
      f.close()
      countPTR = countPTR+1
    currentFile.write("\n\n$INCLUDE /srv/dns-zones/reverse.ipv4/static/%s.%s.%s-static\n" % (nameSplit[0], nameSplit[1], nameSplit[2]))

#For logging
for ip in dict:
  countIP = countIP + 1
#logging.warn("DNS-CRON-REV4[%s]: %s PTR records created from %s IP addresses found in %s config files" % (PID, countPTR, countIP, countFile))

#Running named-checkzone on each reverse-zone file
for zone in open(src_rev4_zones_list):
  arpaZone = '.'.join(zone.strip().split('.')[::-1]) + '.in-addr.arpa.'
  zoneFile = tempZoneDir+zone.strip()
  if call(['named-checkzone', arpaZone, zoneFile]):
    logging.warn("DNS-CRON-REV4[%s]: named-checkzone reported ERROR in zone %s" % (PID, zoneFile))
    countError = countError + 1

#Running nsd-checkzone on each reverse-zone file
for zone in open(src_rev4_zones_list):
  arpaZone = '.'.join(zone.strip().split('.')[::-1]) + '.in-addr.arpa.'
  zoneFile = tempZoneDir+zone.strip()
  if call(['nsd-checkzone', arpaZone, zoneFile]):
    logging.warn("DNS-CRON-REV4[%s]: nsd-checkzone reported ERROR in zone %s" % (PID, zoneFile))
    countError = countError + 1

#Running named-checkconf to verify named config
if call(['named-checkconf',namedConfFile]):
  countError = countError + 1
  logging.warn("DNS-CRON-REV4[%s]: named-checkconf reported ERROR" % (PID))

#Running nsd-checkconf to verify nsd config
if call(['nsd-checkconf',nsdConfFile]):
  logging.warn("DNS-CRON-REV4[%s]: nsd-checkconf reported ERROR" % (PID))
  countError = countError + 1

if os.path.exists(temp_src_rev4_zones_list):
  if os.path.exists(temp_src_rev4_zones_list_old):
    new_hash_src_rev4_zone_list = hashlib.md5(open(temp_src_rev4_zones_list, 'rb').read()).hexdigest()
    old_hash_src_rev4_zone_list = hashlib.md5(open(temp_src_rev4_zones_list_old, 'rb').read()).hexdigest()
    if (new_hash_src_rev4_zone_list == old_hash_src_rev4_zone_list):
      logging.warn("DNS-CRON-REV4[%s]: Contents of %s and %s are identical" % (PID, temp_src_rev4_zones_list, temp_src_rev4_zones_list_old))
      is_src_zone_list_same = 1
    else:
      logging.warn("DNS-CRON-REV4[%s]: Contents of %s and %s are different" % (PID, temp_src_rev4_zones_list, temp_src_rev4_zones_list_old))
      is_src_zone_list_same = 0
  else:
    is_src_zone_list_same = 0
if os.path.exists(all_ptr_ref_list_path):
  if os.path.exists(all_ptr_ref_list_path_old):
    new_hash_all_ptr = hashlib.md5(open(all_ptr_ref_list_path, 'rb').read()).hexdigest()
    old_hash_all_ptr = hashlib.md5(open(all_ptr_ref_list_path_old, 'rb').read()).hexdigest()
    if (new_hash_all_ptr == old_hash_all_ptr):
      logging.warn("DNS-CRON-REV4[%s]: Contents of %s and %s are identical" % (PID, all_ptr_ref_list_path, all_ptr_ref_list_path_old))
      is_all_ptr_same = 1
    else:
      is_all_ptr_same = 0
      logging.warn("DNS-CRON-REV4[%s]: Contents of %s and %s are different" % (PID, all_ptr_ref_list_path, all_ptr_ref_list_path_old))
  else:
    is_all_ptr_same = 0
if (is_all_ptr_same == 1) and (is_src_zone_list_same ==1):
  logging.warn("DNS-CRON-REV4[%s]: PTR records generated in this run are identical to previous run. Run completed." % (PID))
  exit(2)

if countError == 0:
  #Remove all reverse delegation zones from path_to_rev_ipv4_zones that are present in src_rev4_zones_list from prev run
  logger.warn("DNS-CRON-REV4[%s]: Deleting all reverse dns zones (from prev run) listed in source file" % (PID))
  for line in open(src_rev4_zones_list):
    try:
      os.remove(path_to_rev_ipv4_zones+line.strip())
    except OSError:
      pass

  for zoneFile in os.listdir(tempZoneDir):
    os.rename(tempZoneDir+zoneFile,path_to_rev_ipv4_zones+zoneFile)
  logging.warn("DNS-CRON-REV4[%s]: %s PTR records created from %s IP addresses found in %s config files" % (PID, countPTR, countIP, countFile))
  logging.warn("DNS-CRON-REV4[%s]: Reverse PTR generator exited successfully" % (PID))
  exit(0)
else:
  logging.warn("DNS-CRON-REV4[%s]: FAIL - Run aborted due to errors" % (PID))
  exit(1)
