#!/bin/bash


timestamp=$(date +%s)
su - dnsdeploy -c "cd /srv/dns-zones && git add --all && git commit -m 'AUTO COMMIT DNS, Serial No: $timestamp' && git push -u origin master"
if [ $? -eq 0 ]; then
  exit 0
else
  exit 1
fi
