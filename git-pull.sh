#!/bin/bash

VAR="$(su - dnsdeploy -c 'cd /srv/dns-zones && git fetch && git pull' | grep 'up-to-date' | wc -l)"
echo $VAR

if [ $? -eq 0 ]; then
  exit 0
else
  exit 1
fi
