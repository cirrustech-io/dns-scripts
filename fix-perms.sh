#!/bin/bash


dnsZones="/srv/dns-zones"
echo $dnsZones
#Determine DNS Server Application


if [ -d "/etc/bind" ]; then
    chown -R bind:adm $dnsZones
    chmod -R g+w $dnsZones
    exit 0
  elif [ -d "/etc/nsd" ]; then
    chown -R nsd:adm $dnsZones
    chmod -R g+w $dnsZones
    exit 0
  else
    exit 1
fi

