#/bin/bash -x

# set -a is used to export all variables declared into the environment
# set +a closes it
set -a
source /opt/dns-scripts/variables.py
set +a

time_stamp=$(date +%s)

bind_status=$(systemctl status $bind_server &> /dev/null; echo $?)
nsd_status=$(systemctl status $nsd_server &> /dev/null; echo $?)


#If Bind/NSD are not running then exit out
if [ $bind_status -ne 0 ] || [ $nsd_status -ne 0 ];then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "ERROR: BIND9 and/or NSD service not running. Quitting."
  exit 1
fi

#Check if Network Infrastructre Repo has any changes. If no changes found, then exit
git_status_net=$(su - dnsdeploy -c "cd $net_repo_path && git fetch && git status" | grep "behind" | wc -l)
if [ $git_status_net -eq 1 ]; then
  cd $net_repo_path
  sudo -u dnsdeploy git pull
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Pulled latest changes from $net_repo_name" 
else
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "No changes in repo: $net_repo_name"
  exit 1
fi

#Check if DNS Zones repo has any changes. If its behind, then pull and proceed forward. If up-to-date then proceed further. Else exit."
git_status_dns=$(su - dnsdeploy -c "cd $dns_repo_path && git checkout $dev_branch && git fetch && git status")
if [ `echo $git_status_dns | grep "behind" | wc -l` -eq 1 ]; then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "DNS repo is behind. Pulling latest changes"
  cd $net_repo_path
  sudo -u dnsdeploy git pull
elif [ `echo $git_status_dns | egrep "(Changes not staged for commit|Untracked files)" | wc -l` -eq 1 ]; then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "DNS repo has unstaged/uncommitted changes. Check 'git status' for DNS Repo. Not proceeding any further"
  exit 1
elif [ `echo $git_status_dns | grep "up-to-date" | wc -l` -eq 1 ]; then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "DNS repo is already up-to-date. Proceeding forward with subscripts"
else
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "DNS Zones repo, git status is unknown value"
  exit 1
fi

# Execute zone_list_gen.py to recreate named.zones.list and nsd.zones.list
python $zone_list_gen $time_stamp
if [ $? -eq 0 ]; then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "named.zones.list and nsd.zones.list recreated from source files"
else
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "$zone_list_gen encountered an error"
  exit 1
fi

#Execute script to generate PTR records (ipv4)
logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Executing ipv4 PTR generation script"

python $rev_ptr_gen_ipv4 $time_stamp
prevexitcode=$?

#Fixing file permissions
chown -R dnsdeploy.dnsdeploy $dns_repo_path
chmod -R g+w $dns_repo_path
logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Fixed file/folder permissions for $dns_repo_path"


#If PTR generation exited normally then reload dns server
#If reload is not successful then reset all uncommitted changes in dev branch and reload dns server
#If PTR generation did not exit normally, then reset uncommitted changes in dev branch and reload dns server
if [ $prevexitcode -eq 0 ]; then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Executed ipv4 PTR generation script successfully"
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Reloading BIND9 and NSD..."
  systemctl reload $bind_server && systemctl reload $nsd_server
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Reloaded BIND9 and NSD"
  if [ $? -ne 0 ]; then
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "FAILED: Bind9 and/or NSD reload failed"
    #su - dnsdeploy -c "cd $dns_repo_path && git reset --hard HEAD && git clean -fd"
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "WARNING: All local uncommitted changes have been discarded"
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Reloading BIND9 and NSD again..."
    systemctl reload $bind_server && systemctl reload $nsd_server
    if [ $? -eq 0 ]; then
      logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "FAILED: Errors in reverse zones from PTR generation script. BIND and NSD reloaded with last known working config"
      exit 1
    else
      systemctl stop $bind_server
      systemctl stop $nsd_server
      logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "ABORTED: Failed to reload with last known config. BIND9 and NSD service stopped."
      exit 1
    fi
  fi
fi

if [ $prevexitcode -eq 2 ]; then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "COMPLETED: Run completed without changes."
  exit 0
fi

if [ $prevexitcode -eq 1 ]; then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s   "ERROR: Anomolous exit of PTR generation script"
  sudo -u dnsdeploy "cd $dns_repo_path && git checkout $dev_branch && git reset --hard HEAD && git clean -fd"
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "WARNING: All local uncommitted changes in $dev_branch branch have been discarded"
  #
  #Fixing file permissions
  chown -R dnsdeploy.dnsdeploy $dns_repo_path
  chmod -R g+w $dns_repo_path
  #
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Fixed file/folder permissions for $dns_repo_path"
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Reloading BIND9 and NSD..."
  systemctl reload $bind_server && systemctl reload $nsd_server

  if [ $? -eq 0 ]; then
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "BIND9 and NSD reloaded with last known working config"
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "FAILED: Due to non-zero exit status of ipv4 PTR generation script" 
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Run completed with errors" 
    exit 1
  else
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "ERROR: Failed to reload BIND9 and NSD"
    systemctl stop $bind_server
    systemctl stop $nsd_server
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "ABORTED: Could not recover after anomolous exit of PTR script. BIND9 and NSD service stopped."
    logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "FAILED: run was not completed"
    exit 1
  fi

fi

#Commit changes to remote $dev_branch so that the other dns_build server can pull from it
#And test the changes
logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Commiting changes to $dev_branch branch"
sudo -u dnsdeploy "cd $dns_repo_path && git add --all && git commit -m 'DNS config autogenerated with serial number: $time_stamp' && git push -u origin $dev_branch"
if [ $? -eq 0 ]; then
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Committed changes to $dev_branch branch"
  sudo -u dnsdeploy "cd $dns_repo_path && git checkout $master_branch && git merge $dev_branch' && git push -u origin $master_branch"
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "Changes merged with $master_branch branch"
  logger -t "DNS-CRON-WRAPPER[$time_stamp]" -s "SUCCESS: Run completed with changes."
  exit 0
else:
  logger -t ""DNS-CRON-WRAPPER[$time_stamp]" -s "FAILED: Error in committing and merging with $master_branch branch"
