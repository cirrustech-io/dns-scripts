
bind_server="bind9"
nsd_server="nsd"
bind_user="bind"
nsd_user="nsd"

#Temp working directory where the zones will be created before being moved
tempZoneDir='/tmp/dns_working_dir/tempzones/'
dns_working_dir='/tmp/dns_working_dir/'

temp_src_rev4_zones_list="/tmp/dns_working_dir/src.zones.reverse.ipv4.list"
temp_src_rev4_zones_list_old="/tmp/dns_working_dir/src.zones.reverse.ipv4.list.old"

#The file created in each run containing all ipv4 PTR records
all_ptr_ref_list='all_ptr_ref_list'
all_ptr_ref_list_path='/tmp/dns_working_dir/all_ptr_ref_list'

#The ALL PTR file from the previous run is renamed with suffix ".old"
#and used to compare it with the new one from current run
all_ptr_ref_list_path_old='/tmp/dns_working_dir/all_ptr_ref_list.old'

#Location of running-configs of the routers, switches, firewalls
run_config_dir='/srv/network-infrastructure-configuration/data/output/'

#Conf file location for BIND and NSD
namedConfFile='/etc/bind/named.conf'
nsdConfFile='/etc/nsd/nsd.conf'
nsd_zones_list_file="/srv/dns-zones/includes-nsd/nsd.zones.list"
bind_zones_list_file="/srv/dns-zones/includes-bind/named.zones.list"

#Autonomous System Numbers (ASN)
infonas_asn='35313'
infonas_uk_asn='51406'

#Repo names
net_repo_name="network-infrastructure-configuration"
dns_repo_name="dns-zones"

#Repo paths
dns_repo_path='/srv/dns-zones/'
net_repo_path='/srv/network-infrastructure-configuration/'

#Scripts Path
rev_ptr_gen_ipv4="/opt/dns-scripts/rev-zone-gen-ipv4.py"
zone_list_gen="/opt/dns-scripts/zones-list-gen.py"
reload_dns_service="/opt/dns-scripts/reload.sh"

#GIT branches
dev_branch="dev"
master_branch="master"

#Initializing empty variables. Should be blank at the start of each run.
dict={}
hostname=''

#Path of data source files from which zone files and (named|nsd).zone.list are generated
src_fwd_zones_list="/srv/dns-zones/src.zones.forward.list" 
src_rev4_zones_list="/srv/dns-zones/src.zones.reverse.ipv4.list"
src_rev6_zones_list="/srv/dns-zones/src.zones.reverse.ipv6.list"

#Path to Zones' folders
path_to_fwd_zones="/srv/dns-zones/forward/"
path_to_rev_ipv4_zones="/srv/dns-zones/reverse.ipv4/generated/"
path_to_rev_ipv6_zones="/srv/dns-zones/reverse.ipv6/"

