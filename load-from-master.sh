#!/bin/bash

set -a
source /opt/dns-scripts/variables.py
set +a

if [ -d "/etc/bind" ]; then
  dnsServer="bind9"
elif [ -d "/etc/nsd" ]; then
  dnsServer="nsd"
else
  logger -t "DNS-CRON-DL-MASTER[$time_stamp]" -s "ERROR: Unknown DNS server. Quitting."
  exit 1
fi 

time_stamp=$(date +%s)
git_status_check=$(su - dnsdeploy -c "cd $dns_repo_path && git checkout $master_branch && git fetch && git status | grep 'behind' | wc -l")

if [[ "$git_status_check" == 1 ]]; then
  sudo -u dnsdeploy "cd $dns_repo_path && git pull"
  logger -t "DNS-CRON-DL-MASTER[$time_stamp]" -s "Pulled latest changes from $dns_repo_name" 
  bash reload.sh
  systemctl reload $dnsServer
  if [ $? -eq 0 ];then
    logger -t "DNS-CRON-DL-MASTER[$time_stamp]" -s "SUCCESS: $dnsServer reloaded successfully with new changes"
    exit 0
  else
    logger -t "DNS-CRON-DL-MASTER[$time_stamp]" -s "FAILED: $dnsServer could not be reloaded with new changes"
    exit 1
  fi
else
  logger -t "DNS-CRON-DL-MASTER[$time_stamp]" -s "No changes in repo: $dns_repo_name"
  exit 0
fi
